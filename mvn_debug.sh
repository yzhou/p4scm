#!/usr/bin/env bash

echo "Set Maven Remote Debug:" $1

if [ "$1" = "off" ]; then
    echo "Turn off debug"
    export MAVEN_OPTS=
else
    echo "Turn on debug"
    export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compile=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"
fi
