package org.jenkinsci.plugins.p4scm.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CommonUtilTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDoesFilenameMatchP4Pattern() {
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/...",true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot3/somefile/testfile",
                "//depot/...",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/.../testfile",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/*/testfile",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/some*/...",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/*file...",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/.../*",true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/somefile/test",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/somefile/testfile",true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/.../test",true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/.../*test",true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/somefile/testfile",
                "//depot/.../file*",true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/SomeFile/testFile",
                "//depot/s.../testfile", false));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/SomeFile/testFile",
                "//depot/S%%1e/testFile", true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/SomeFile/testFile",
                "//depot/%%9/testFile", true));
        assertEquals(false, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/SomeFile/testFile",
                "//depot/%%9", true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "//depot/Some File/testFile",
                "\"//depot/Some File/testFile\"", true));
        assertEquals(true, CommonUtil.doesFilenameMatchP4Pattern(
                "\"//depot/Some File/testFile\"",
                "//depot/Some File/testFile", true));
    }

}
