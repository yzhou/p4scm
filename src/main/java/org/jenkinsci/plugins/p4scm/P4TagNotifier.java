package org.jenkinsci.plugins.p4scm;

import java.io.IOException;

import jenkins.model.Jenkins;

import org.jenkinsci.plugins.p4scm.utils.MacroStringHelper;
import org.jenkinsci.plugins.p4scm.utils.ParameterSubstitutionException;
import org.kohsuke.stapler.DataBoundConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hudson.EnvVars;
import hudson.Extension;
import hudson.Launcher;
import hudson.model.BuildListener;
import hudson.model.Result;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;

@SuppressWarnings("unchecked")
public class P4TagNotifier extends Notifier {
    
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(P4TagNotifier.class);

    public String rawLabelName;
    public String rawLabelOwner;
    public String rawLabelDesc;
    public boolean onlyOnSuccess = true;
    
    @DataBoundConstructor
    public P4TagNotifier(String rawLabelName, String rawLabelDesc, String rawLabelOwner, boolean onlyOnSuccess){
        this.rawLabelName = rawLabelName;
        this.rawLabelDesc = rawLabelDesc;
        this.rawLabelOwner = rawLabelOwner;
        this.onlyOnSuccess = onlyOnSuccess;
    }

    public boolean isOnlyOnSuccess() {
        return onlyOnSuccess;
    }

    public void setOnlyOnSuccess(boolean onlyOnSuccess) {
        this.onlyOnSuccess = onlyOnSuccess;
    }

    public String getRawLabelName() {
        return rawLabelName;
    }

    public void setRawLabelName(String rawLabelName) {
        this.rawLabelName = rawLabelName;
    }

    public String getRawLabelDesc() {
        return rawLabelDesc;
    }

    public void setRawLabelDesc(String rawLabelDesc) {
        this.rawLabelDesc = rawLabelDesc;
    }

    public String getRawLabelOwner() {
        return rawLabelOwner;
    }

    public void setRawLabelOwner(String rawLabelOwner) {
        this.rawLabelOwner = rawLabelOwner;
    }
    
    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return BuildStepMonitor.NONE;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean perform(AbstractBuild<?,?> build, Launcher launcher, BuildListener listener) throws InterruptedException {
        if(!onlyOnSuccess || build.getResult() == Result.SUCCESS){

            EnvVars environment;
            try{
                environment = build.getEnvironment(listener);
            } catch (IOException e) {
                listener.getLogger().println("Could not load build environment.");
                return false;
            }

            P4TagAction tagAction = (P4TagAction) build.getAction(P4TagAction.class);

            if(tagAction == null){
                //look for Promoted build
                listener.getLogger().println("Could not find tag information, checking if this is a promotion job.");
                String jobName = environment.get("PROMOTED_JOB_NAME");
                String buildNumber = environment.get("PROMOTED_NUMBER");
                if(jobName == null || jobName.isEmpty() || buildNumber == null || buildNumber.isEmpty()){
                    listener.getLogger().println("Not a promotion job.");
                } else {
                    AbstractProject project = (AbstractProject) Jenkins.getInstance().getItemByFullName(jobName, AbstractProject.class);
                    int buildNum = Integer.parseInt(buildNumber);
                    build = (AbstractBuild<?, ?>) project.getBuildByNumber(buildNum);
                    tagAction = (P4TagAction) build.getAction(P4TagAction.class);   
                }
                if(tagAction == null){
                    listener.getLogger().println("Could not label build in perforce; is it a valid perforce job?");
                    return false;
                }
            }
            
            P4SCM scm = tagAction.getSCM();
            if (scm == null) {
                listener.getLogger().println("Cannot retrieve the Perforce SCM");
                return false;
            }

            listener.getLogger().println("Labelling Build in Perforce using " + rawLabelName);

            String labelName, labelDesc, labelOwner;
            try {
                labelName = MacroStringHelper.substituteParameters(rawLabelName, scm, build, null);
                labelDesc = MacroStringHelper.substituteParameters(rawLabelDesc, scm, build, null);
                labelOwner = MacroStringHelper.substituteParameters(rawLabelOwner, scm, build, null);
            } catch (ParameterSubstitutionException ex) {
                listener.getLogger().println("Parameter substitution error in label items. "+ex.getMessage());
                return false;
            }
            
            if (labelName == null || labelName.equals("")) {
                listener.getLogger().println("Label Name is empty, cannot label.");
                return false;
            }
            
            if (labelDesc == null || labelDesc.equals("")) {
                labelDesc = "Label automatically generated by Perforce Plugin.";
            }
            

            try {
                tagAction.tagBuild(labelName, labelDesc, labelOwner);
            } catch (IOException e) {
                listener.getLogger().println(e.getMessage());
                return false;
            }

            listener.getLogger().println("Label '" + labelName + "' successfully generated.");
        }

        return true;
    }

    public static DescriptorImpl descriptor() {
        return Jenkins.getInstance().getDescriptorByType(P4TagNotifier.DescriptorImpl.class);
    }

    @Extension
    public static final class DescriptorImpl extends BuildStepDescriptor<Publisher> {

        @SuppressWarnings("rawtypes")
        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Create or Update Label in Perforce";
        }
        
    }
}
