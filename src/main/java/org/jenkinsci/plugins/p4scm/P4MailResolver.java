package org.jenkinsci.plugins.p4scm;

import java.io.OutputStream;

import jenkins.model.Jenkins;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hudson.FilePath;
import hudson.model.TaskListener;
import hudson.model.TopLevelItem;
import hudson.model.AbstractProject;
import hudson.model.Node;
import hudson.model.User;
import hudson.Launcher;
import hudson.tasks.MailAddressResolver;
import hudson.util.StreamTaskListener;

public class P4MailResolver extends MailAddressResolver {

    private static final Logger logger = LoggerFactory.getLogger(P4MailResolver.class);
    
    
    @Override
    public String findMailAddressFor(User u) {
        String email = findPerforceMailAddressFor(u);
        if (email == null){
            return null;
        } else if (email.matches(".+@.+")){
            return email;
        } else {
            logger.debug("Rejecting invalid email ("+ email +") retrieved from perforce.");
            return null;
        }
    }
    
    public String findPerforceMailAddressFor(User u) {
        logger.info("Email address for " + u.getId() + " requested.");
        String perforceId = u.getId();
        P4UserProperty puprop = u.getProperty(P4UserProperty.class);
        if (puprop != null){
            if(puprop.getPerforceId() != null){
                logger.debug("Using perforce user id '" + perforceId + "' from " + u.getId() + "'s properties.");
                perforceId = puprop.getPerforceId();
            }
            if(puprop.getPerforceEmail() != null){
                logger.info("Got email ("+puprop.getPerforceEmail()+") from " + u.getId() +"'s P4 properties.");
                return puprop.getPerforceEmail();
            }
        }
        for (AbstractProject<?,?> p : Jenkins.getInstance().getAllItems(AbstractProject.class)) {
            if (!(p instanceof TopLevelItem)) continue;
            if (p.isDisabled()) continue;
            if (p.getScm() instanceof P4SCM) {
                logger.info("Checking " + p.getName() + "'s Perforce SCM for " + perforceId + "'s address.");
                P4SCM pscm = (P4SCM) p.getScm();
                TaskListener listener = new StreamTaskListener((OutputStream)System.out);
                Node node = p.getLastBuiltOn();

                // If the node is offline, skip the project.
                // The node needs to be online for us to execute commands.
                if (node == null) {
                    logger.info("Build doesn't seem to have been run before. Cannot resolve email address using this project.");
                    continue;
                }
                if (node.getChannel() == null) {
                    logger.info("Node " + node.getDisplayName() + " is not up, cannot resolve email address using this project.");
                    continue;
                }
                // TODO: replace this with p.getLastBuild().getWorkspace()
                // which is the way it should be, but doesn't work with this version of hudson.
                for (int tries = 0; tries < 5; tries++) {
                    FilePath workspace = p.getLastBuiltOn().getRootPath();
                    Launcher launcher = p.getLastBuiltOn().createLauncher(listener);
                    com.tek42.perforce.model.User pu = null;
                    try {
                        logger.debug("Trying to get email address from perforce for " + perforceId);
                        pu = pscm.getDepot(launcher, workspace, p, null, node).getUsers().getUser(perforceId);
                        if (pu != null && pu.getEmail() != null && !pu.getEmail().equals("")) {
                            logger.debug("Got email (" + pu.getEmail() + ") from perforce for " + perforceId);
                            return pu.getEmail();
                        } else {
                            //operation succeeded, but no email address was found for this user
                            return null;
                        }
                    } catch (Exception e) {
                        logger.debug("Could not get email address from Perforce: " + e.getMessage());
                        e.printStackTrace(listener.getLogger());
                    }
                    try {
                        //gradually increase sleep time
                        Thread.sleep(tries);
                    } catch (InterruptedException e){
                        return null;
                    }
                }
            }
        }
        return null;
    }

}
