package org.jenkinsci.plugins.p4scm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.kohsuke.stapler.export.Exported;

import com.tek42.perforce.model.Changelist;

import hudson.model.User;
import hudson.scm.ChangeLogSet;

public class P4ChangeLogEntry extends ChangeLogSet.Entry {
    
    Changelist change;
    
    public P4ChangeLogEntry(P4ChangeLogSet parent) {
        super();
        setParent(parent);
    }

    @Override
    public String getMsg() {
        return change.getDescription();
    }

    @Override
    public User getAuthor() {
        User author = User.get(change.getUser());
        return author;
    }

    @Override
    public Collection<String> getAffectedPaths() {
        List<String> paths = new ArrayList<String>(change.getFiles().size());
        for (Changelist.FileEntry entry : change.getFiles()) {
            //only report those files that are actually in the workspace
            if(entry.getWorkspacePath()!=null && !entry.getWorkspacePath().equals("")){
                paths.add(entry.getWorkspacePath());
            }
        }
        return paths;
    }
    
    public Collection<Changelist.FileEntry> getAffectedFiles() {
        return change.getFiles();
    }
    

    public String getUser() {
        return getAuthor().getDisplayName();
    }

    @Exported
    public String getChangeNumber() {
        return new Integer(getChange().getChangeNumber()).toString();
    }

    //used for email-ext
    public String getCommitId() {
        return getChangeNumber();
    }

    @Exported
    public String getChangeTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(getChange().getDate());
    }

    //used for email-ext
    public long getTimestamp() {
        return getChange().getDate().getTime();
    }

    /**
     * {@inheritDoc}
     */
    public String getCurrentRevision() {
        return getChangeNumber();
    }

    /**
     * @return the change
     */
    public Changelist getChange() {
        return change;
    }

    /**
     * @param change the change to set
     */
    public void setChange(Changelist change) {
        this.change = change;
    }

}
