package org.jenkinsci.plugins.p4scm;

import java.io.IOException;
import java.net.URL;

import com.tek42.perforce.model.Changelist;

import hudson.scm.RepositoryBrowser;


public abstract class P4RepositoryBrowser extends RepositoryBrowser<P4ChangeLogEntry> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Determines the link to the diff between the version.
     * in the {@link P4ChangeLogEntry.Change.File} to its previous version.
     *
     * @return
     *      null if the browser doesn't have any URL for diff.
     */
    public abstract URL getDiffLink(Changelist.FileEntry file) throws IOException;

    /**
     * Determines the link to a single file under Perforce.
     * This page should display all the past revisions of this file, etc.
     *
     * @return
     *      null if the browser doesn't have any suitable URL.
     */
    public abstract URL getFileLink(Changelist.FileEntry file) throws IOException;

}
