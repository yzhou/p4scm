package org.jenkinsci.plugins.p4scm;

import org.kohsuke.stapler.DataBoundConstructor;

import hudson.Extension;
import hudson.Util;
import hudson.model.UserProperty;
import hudson.model.UserPropertyDescriptor;
import hudson.model.User;

public class P4UserProperty extends UserProperty {

    private String perforceId;
    private String perforceEmail;

    public P4UserProperty(){
        super();
    }

    @DataBoundConstructor
    public P4UserProperty(String perforceId){
        super();
        this.perforceId = Util.fixEmptyAndTrim(perforceId);
    }

    public String getPerforceId() {
        return perforceId;
    }

    public void setPerforceId(String perforceId) {
        this.perforceId = perforceId;
    }

    @Extension
    public static class DescriptorImpl extends UserPropertyDescriptor{

        @Override
        public UserProperty newInstance(User user) {
            return new P4UserProperty();
        }

        @Override
        public String getDisplayName() {
            return "Perforce";
        }
        
    }

    public String getPerforceEmail() {
        return perforceEmail;
    }

    public void setPerforceEmail(String perforceEmail) {
        this.perforceEmail = perforceEmail;
    }
}
