package org.jenkinsci.plugins.p4scm;

import java.io.File;
import java.io.FileFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WipeWorkspaceExcludeFilter implements FileFilter, Serializable {

    private static final long serialVersionUID = 1L;
    
    private final List<String> excluded = new ArrayList<String>();

    public WipeWorkspaceExcludeFilter(String... args) {
        excluded.addAll(Arrays.asList(args));
    }

    public void exclude(String arg) {
        excluded.add(arg);
    }

    @Override
    public boolean accept(File pathname) {
        for (String exclude : excluded) {
            if (pathname.getName().equals(exclude)) {
                return false;
            }
        }
        return true;
    }

}
