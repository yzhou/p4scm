package org.jenkinsci.plugins.p4scm;

import java.io.InputStream;
import java.io.OutputStream;

import com.tek42.perforce.process.Executor;

public interface P4Executor extends Executor {
    
    //void close();
    
    //void exec(String[] cmd) throws PerforceException;

    InputStream getInputStream();

    OutputStream getOutputStream();
}
