package org.jenkinsci.plugins.p4scm;

import java.io.IOException;

import hudson.Proc;
import hudson.remoting.FastPipedOutputStream;

public class JenkinsPipedOutputStream extends FastPipedOutputStream {
    
    
    /** Waiting jenkins process finishes, and close stream in another thread
     * @param process
     */
    public void closeOnProcess(final Proc process) {
        final JenkinsPipedOutputStream stream = this;
        
        new Runnable() {
            
            @Override
            public void run() {
                
                try {
                    process.join();
                    stream.flush();
                } catch (IOException | InterruptedException e) {
                    //e.printStackTrace();
                } finally {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        //e.printStackTrace();
                    }
                } 
            }
            
            void start() {
                new Thread(this).start();
            }
        }.start();
        
    }

}
