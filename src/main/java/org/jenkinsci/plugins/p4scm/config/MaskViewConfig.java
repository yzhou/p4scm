package org.jenkinsci.plugins.p4scm.config;

import org.kohsuke.stapler.DataBoundConstructor;

public class MaskViewConfig {

    String viewMask;
    boolean useViewMaskForPolling;
    boolean useViewMaskForSyncing;
    boolean useViewMaskForChangeLog;

    @DataBoundConstructor
    public MaskViewConfig(String viewMask, boolean useViewMaskForPolling, 
            boolean useViewMaskForSyncing, boolean useViewMaskForChangeLog) {
        this.viewMask = viewMask;
        this.useViewMaskForPolling = useViewMaskForPolling;
        this.useViewMaskForSyncing = useViewMaskForSyncing;
        this.useViewMaskForChangeLog = useViewMaskForChangeLog;
    }

    public String getViewMask() {
        return viewMask;
    }

    public boolean isUseViewMaskForPolling() {
        return useViewMaskForPolling;
    }

    public boolean isUseViewMaskForSyncing() {
        return useViewMaskForSyncing;
    }

    public boolean isUseViewMaskForChangeLog() {
        return useViewMaskForChangeLog;
    }

    @Override
    public String toString() {
        return String.format("%s:\n(viewMask: %s\n, useViewMaskForPolling:%s, "
                + "useViewMaskForSyncing:%s, useViewMaskForChangeLog:%s)", 
                MaskViewConfig.class.getSimpleName(), 
                getViewMask(),
                isUseViewMaskForPolling(),
                isUseViewMaskForSyncing(),
                isUseViewMaskForChangeLog());
    }
    
    
}
