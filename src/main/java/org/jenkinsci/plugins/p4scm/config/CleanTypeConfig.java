package org.jenkinsci.plugins.p4scm.config;

import org.kohsuke.stapler.DataBoundConstructor;


/**
 * Contains workspace cleanup options for {@link P4SCM}.
 * @author Oleg Nenashev <nenashev@synopsys.com>, Synopsys Inc.
 * @since TODO: define a version
 */
public class CleanTypeConfig {

    String value;
    boolean restoreChangedDeletedFiles;

    @DataBoundConstructor
    public CleanTypeConfig(String value, Boolean restoreChangedDeletedFiles) {
        this.value = value;
        this.restoreChangedDeletedFiles = restoreChangedDeletedFiles != null ? restoreChangedDeletedFiles.booleanValue() : false;
    }
    
    public boolean isQuick() {
        return value.equals("quick");
    }
    
    public boolean isWipe() {
        return value.equals("wipe");
    }

    public boolean isRestoreChangedDeletedFiles() {
        return restoreChangedDeletedFiles;
    }

    @Override
    public String toString() {
        return String.format("CleanTypeConfig(Value: %s, restoreChangedDeletedFiles: %s)", value, restoreChangedDeletedFiles);
    }

}
