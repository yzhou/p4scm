package org.jenkinsci.plugins.p4scm.config;

import org.kohsuke.stapler.DataBoundConstructor;

public class WorkspaceCleanupConfig {

    CleanTypeConfig cleanType;
    boolean wipeRepoBeforeBuild;

    @DataBoundConstructor
    public WorkspaceCleanupConfig(CleanTypeConfig cleanType, boolean wipeRepoBeforeBuild) {
        this.cleanType = cleanType;
        this.wipeRepoBeforeBuild = wipeRepoBeforeBuild;
    }

    public CleanTypeConfig getCleanType() {
        return cleanType;
    }

    public boolean isWipeRepoBeforeBuild() {
        return wipeRepoBeforeBuild;
    }

    @Override
    public String toString() {
        return String.format("WorkspaceCleanupConfig(cleanType: %s, wipeRepoBeforeBuild: %s)", cleanType, wipeRepoBeforeBuild);
    }
    
    
}
