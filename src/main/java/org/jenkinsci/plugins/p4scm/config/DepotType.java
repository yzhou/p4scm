package org.jenkinsci.plugins.p4scm.config;

import org.kohsuke.stapler.DataBoundConstructor;

public class DepotType {

    public static final String USE_P4STREAM_MARKER="stream";
    public static final String USE_CLIENTSPEC_MARKER="file";
    public static final String USE_PROJECTPATH_MARKER="map";
    
    String value;
    String p4Stream;
    String clientSpec;
    String projectPath;
    
    @DataBoundConstructor
    public DepotType(String value, String p4Stream, String clientSpec, String projectPath) {
        this.value = (value != null) ? value : "";
        this.p4Stream = p4Stream;
        this.clientSpec = clientSpec;
        this.projectPath = projectPath;
    }
    
    public String getProjectPath() {
        return projectPath;
    }
    
    @Override
    public String toString() {
        return String.format("%s: \n%s", value, useP4Stream()? p4Stream : useProjectPath()? projectPath : useClientSpec()? clientSpec : "");
    }

    public boolean useProjectPath() {
        return value.equals(USE_PROJECTPATH_MARKER);
    }

    public String getClientSpec() {
        return clientSpec;
    }

    public boolean useClientSpec() {
        return value.equals(USE_CLIENTSPEC_MARKER);
    }
    
    public String getP4Stream() {
        return p4Stream;
    }

    public boolean useP4Stream() {
        return value.equals(USE_P4STREAM_MARKER);
    }
}
