package org.jenkinsci.plugins.p4scm.utils;

import java.io.IOException;

@SuppressWarnings("serial")
public class ParameterSubstitutionException extends IOException {

    public String parameterString;

    public String getParameterString() {
        return parameterString;
    }

    public ParameterSubstitutionException(String parameterString, String message) {
        super(message);
        this.parameterString = parameterString;
    }

    public ParameterSubstitutionException(String parameterString, Throwable cause) {
        super(cause);
        this.parameterString = parameterString;
    }

    @Override
    public String getMessage() {
        return String.format("<%s>: %s", parameterString, super.getMessage());
    }
    
    
}
