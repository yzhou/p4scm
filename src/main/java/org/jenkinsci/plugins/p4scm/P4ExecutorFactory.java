package org.jenkinsci.plugins.p4scm;

import hudson.FilePath;
import hudson.Launcher;
import hudson.Launcher.RemoteLauncher;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.tek42.perforce.process.Executor;
import com.tek42.perforce.process.ExecutorFactory;

public class P4ExecutorFactory implements ExecutorFactory {
    
    transient private Launcher launcher;
    transient private Map<String, String> env;
    transient private FilePath filePath;

    public P4ExecutorFactory(Launcher launcher, FilePath filePath) {
        
        this.launcher = launcher;
        this.filePath = filePath;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void setEnv(Map<String, String> env) {
        this.env = new HashedMap(env);
        //Clear the P4CONFIG field to eliminate issues with configuration files
        this.env.put("P4CONFIG", ""); //TODO: what is the usage of P4CONFIG
    }

    @Override
    public Executor newExecutor() {
        if(launcher instanceof RemoteLauncher) {
            return new P4RemoteExecutor(launcher, env, filePath);
        } else {
            return new P4DefaultExecutor(launcher, env, filePath);
        }
    }

}
